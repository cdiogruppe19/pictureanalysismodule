package finders;

import analysis.FeatureAnalyzer;
import analysis.ImageConverter;
import analysis.QRReader;
import features.Feature;
import analysis.CircleDetection;
import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class FeatureFinder {

    private List<FeatureAnalyzer> analyzers = new ArrayList<>();
    private HashMap<String,Long> runTimes = new HashMap<>();

    public FeatureFinder(){
        analyzers.add(new QRReader());
        analyzers.add(new CircleDetection());
    }

    public FeatureFinder(List<FeatureAnalyzer> analyzers){
        this.analyzers=analyzers;
    }

    public List<Feature> findFeatures(BufferedImage img){
        List<Feature> features = new ArrayList<>();

        runTimes.clear();

        for(FeatureAnalyzer a:analyzers) {
            try {
                long start = System.currentTimeMillis();
                features.addAll(a.findFeature(img));
                runTimes.put(a.getFeatureName(),System.currentTimeMillis()-start);

            } catch (NullPointerException e) {
            }
        }

        return features;
    }

    public HashMap<String,Long> getLastRuntimes(){
        return runTimes;
    }


}
