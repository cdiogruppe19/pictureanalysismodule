package analysis;

import com.google.zxing.*;
import com.google.zxing.client.j2se.BufferedImageLuminanceSource;
import com.google.zxing.common.HybridBinarizer;
import com.google.zxing.multi.qrcode.QRCodeMultiReader;
import features.Feature;
import features.QRCode;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.List;

public class QRReader implements FeatureAnalyzer{
    //Credit for the guide and library usage:
    //https://www.callicoder.com/qr-code-reader-scanner-in-java-using-zxing/


    @Override
    public List<Feature> findFeature(BufferedImage img) {

        LuminanceSource source = new BufferedImageLuminanceSource(img);
        BinaryBitmap bitmap = new BinaryBitmap(new HybridBinarizer(source));

        try{
            Result[] results = new QRCodeMultiReader().decodeMultiple(bitmap);
            List<Feature> qrList =new ArrayList<Feature>();
            for(Result result:results) {
                ResultPoint[] resultPoints = result.getResultPoints();

                Point p = new Point((int) resultPoints[1].getX(), (int) resultPoints[1].getY());

                int width = (int) resultPoints[2].getX() - p.x;
                int height = (int) resultPoints[0].getY() - p.y;

                QRCode myQR = new QRCode(p, width, height,result.getText());
                qrList.add(myQR);
            }
            return qrList;
        }catch(NotFoundException e){
            //System.out.println("Could not find a QR code.");
            return null;
        }
    }

    @Override
    public String getFeatureName() {
        return "QR code";
    }
}
