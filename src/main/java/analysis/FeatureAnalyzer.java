package analysis;

import features.Feature;

import java.awt.image.BufferedImage;
import java.util.List;

public interface FeatureAnalyzer {

    List<Feature> findFeature(BufferedImage img);
    String getFeatureName();
}
