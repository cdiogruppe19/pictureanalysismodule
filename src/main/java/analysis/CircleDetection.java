package analysis;

import features.Feature;
import features.Ring;
import org.opencv.core.Core;
import org.opencv.core.Mat;
import org.opencv.imgproc.Imgproc;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.List;

public class CircleDetection implements FeatureAnalyzer{
    static{System.loadLibrary(Core.NATIVE_LIBRARY_NAME);}

    double scaling;

    public CircleDetection(double scaling){
        this.scaling=scaling;
    }

    public CircleDetection(){
        this(1.0);
    }

    @Override
    public List<Feature> findFeature(BufferedImage img) {
        if(scaling!=1.0){
            int scaleWidth=(int)(scaling*img.getWidth());
            int scaleHeight=(int)(scaling*img.getHeight());

            Image scaledImg = img.getScaledInstance(scaleWidth,scaleHeight,Image.SCALE_FAST);
            img = new BufferedImage(scaleWidth,scaleHeight,BufferedImage.TYPE_3BYTE_BGR);
            img.getGraphics().drawImage(scaledImg,0,0,null);
        }

        Mat src = ImageConverter.bufferedImageToMat(img);
        Mat srcgray=new Mat();
        if(src.empty()){
            return null;
        }
        Imgproc.cvtColor(src,srcgray,Imgproc.COLOR_RGB2GRAY);

        Mat circles = new Mat();
        Imgproc.HoughCircles(srcgray,circles,Imgproc.HOUGH_GRADIENT,1,srcgray.rows()/8,200,100,0,0);

        List<Feature> rings = new ArrayList<Feature>();
        for(int i =0; i<circles.size().width; i++){
            Mat circle = circles.col(i);

            double[] arr = circle.get(0,0);
            Point center = new Point((int)(Math.round(arr[0])/scaling),(int)(Math.round(arr[1])/scaling));
            int radius = (int)(Math.round(arr[2])/scaling);

            Point upLeftCorner = new Point(center.x-radius,center.y-radius);
            Ring currentRing = new Ring(upLeftCorner,radius*2,radius*2,radius,radius,0);
            rings.add(currentRing);
        }
        return rings;
    }

    @Override
    public String getFeatureName() {
        return "Circle";
    }
}
