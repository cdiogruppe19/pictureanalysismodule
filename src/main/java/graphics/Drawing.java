package graphics;

import features.Feature;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.util.List;


public class Drawing extends Canvas {

    List<Feature> features;
    BufferedImage img;

    Drawing(List<Feature> features, BufferedImage img){
        this.features=features;
        this.img=img;
    }

    public void paint(Graphics g){
        g.drawImage(img,0,0,null);

        ((Graphics2D)g).setStroke(new BasicStroke(4));
        for(Feature f : features){
            f.draw(g);
        }
    }

}
