package graphics;



import analysis.CircleDetection;
import analysis.FeatureAnalyzer;
import analysis.QRReader;
import finders.FeatureFinder;
import features.Feature;
import org.opencv.core.Core;
import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class Tester {
    static{System.loadLibrary(Core.NATIVE_LIBRARY_NAME);}



    public static void main(String[] args) throws Exception{
       new Tester().test();

    }

    public void test() throws IOException {
        //File file = new File("src/main/resources/qr/reasonable_lighting.jpg");
        //File file = new File("src/main/resources/circles/Olympic.jpg");
        File file = new File("src/main/resources/hybrids/Plates.png");
        BufferedImage myImage = ImageIO.read(file);
        List<FeatureAnalyzer> analyzers = new ArrayList<>();
        analyzers.add(new QRReader());
        analyzers.add(new CircleDetection(1.0));

        FeatureFinder finder =new FeatureFinder(analyzers);


        //Test speed
        int n=300;
        System.out.println("Starting test expect "+ (n*200.0)/1000.0 + " seconds of wait");
        long start= System.currentTimeMillis();
        for(int i=0;i<n;i++){
            finder.findFeatures(myImage);
        }
        double average = (System.currentTimeMillis()-start)/(double)n;
        System.out.printf("Average analysis Time: %.2fms(%.2f fps)\n",average, 1000.0/average);

        System.out.println();
        System.out.println("Final image times:");

        List<Feature> features=finder.findFeatures(myImage);
        for(String key :finder.getLastRuntimes().keySet()){
            System.out.println(key+": " + finder.getLastRuntimes().get(key)+" ms");
        }

        //Draw image
        JFrame frame = new JFrame("Converted Image");
        Canvas canvas = new Drawing(features,myImage);
        canvas.setSize(myImage.getWidth(),myImage.getHeight());
        frame.add(canvas);
        frame.pack();
        frame.setVisible(true);
    }



}
