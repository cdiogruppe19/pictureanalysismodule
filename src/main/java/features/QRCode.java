package features;

import features.Feature;

import java.awt.*;


public class QRCode extends Feature {

    String value;

    public QRCode(Point pos, int width, int height, String value) {
        super(pos, width, height);
        this.value = value;
    }

    @Override
    public String toString() {
        return super.toString()+"features.QRCode{" +
                "value='" + value + '\'' +
                '}';
    }

    @Override
    public void draw(Graphics g){
        g.setColor(Color.BLUE);

        g.drawRect(pos.x,pos.y,width,height);

        g.drawString(value,pos.x+5,pos.y+height/2);
    }
}
