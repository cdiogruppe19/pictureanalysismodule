package features;

import java.awt.*;

public class Ring extends Feature {

    private double a;
    private double b;
    private double rotation;


    public Ring(Point pos, int width, int height, double a, double b, double rotation) {
        super(pos, width, height);
        this.a=a;
        this.b=b;
        this.rotation=rotation;
    }

    public double getA() {
        return a;
    }

    public void setA(double a) {
        this.a = a;
    }

    public double getRotation() {
        return rotation;
    }

    public void setRotation(double rotation) {
        this.rotation = rotation;
    }

    public double getB() {
        return b;
    }

    public void setB(double b) {
        this.b = b;
    }

    @Override
    public void draw(Graphics g){
        g.setColor(Color.RED);

        g.drawOval(pos.x,pos.y,width,height);

        g.setColor(Color.GREEN);

        g.drawRect((int)(pos.x+a),(int)(pos.y+b),3,3);
    }
}
