package features;

import java.awt.*;

public abstract class Feature {

    //Up left corner
    protected Point pos;

    protected int width;
    protected int height;

    public Feature(Point pos, int width, int height) {
        this.pos = pos;
        this.width = width;
        this.height = height;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public Point getPos() {
        return pos;
    }

    public void setPos(Point pos) {
        this.pos = pos;
    }

    @Override
    public String toString() {
        return "Feature{" +
                "pos=" + pos +
                ", width=" + width +
                ", height=" + height +
                '}';
    }

    public void draw(Graphics g){
        Color c =g.getColor();

        g.setColor(Color.RED);

        g.drawRect(pos.x,pos.y,width,height);
        g.setColor(c);

    }
}
